package com.janiszewski.lab;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReservationsDataUploaderTest {

	@Test
	public void test() throws Exception {
		ReservationsDataUploader.main(new String[0]);
		assertEquals(1000, ReservationsDataUploader.numItemsAdded, 0);
	}
}
